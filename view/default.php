<main role="main" class="container-fluid">
<?php

use app\Helpers\Output;
use app\Helpers\Text;


Output::manageAlerts();

if (!empty($_GET['view']) && $_GET['view'] != 'view/default') {
   
        Output::getContent($_GET['view']);
 
} else {
    Output::render('messageBox', 'Welcome to our site!', 'info');
}
?>
</main>