
 
$(document).ready( function ($) {


    let btncl = document.querySelector('#button-courses-list');
    
    if (btncl) {
        btncl.addEventListener('click', event => {
           
            let courseslist = document.querySelector('#courses-list').classList;
            if (courseslist.contains('bgblue')) {
                courseslist.replace('bgblue','bgyellow');
            } else {
                courseslist.remove('bgyellow');
                courseslist.add('bgblue');
            }
        });
    }

    
    window.addEventListener("load", function(e) {
        
        let selector = document.querySelector("#cc-formation");
        if (typeof(selector) != 'undefined' && selector != null) {
            selector.addEventListener("change", function (e) {
                
                e.preventDefault();
                
                fetch("api/route/course/getByFormationForForm/" + selector.value, {
                    method: "GET",
                    headers: {"Content-Type": "application/x-www-form-urlencoded"},
                }).then(function (response) {
                    return response.text();
                }).then(function (response) {
                    let courseList = document.querySelector("#cc-course");
                    courseList.innerHTML = response.toString();
                }).catch((error) => {
                    console.log(error.toString());
                });
            });
        }

        

        const includeMenu = document.querySelector(".include-admin-menu");
        if (typeof(includeMenu) != 'undefined' && includeMenu != null) {
            fetch("view/admin/menu.html")
                .then(function (response) {
                    return response.text();
                }).then(function (response) {
                    includeMenu.innerHTML = response.toString();
                }).catch((error) => {
                    console.log(error.toString());
                });
        }

        /**
         * UsersList : Modals
         * @type {NodeListOf<Element>}
         */
        let callerid = document.querySelector("#m-uid");
        let callertoken = document.querySelector("#m-tokenu");
        let modalBody = document.querySelector("#modal-user-list-body");
        let userModalLinks = document.querySelectorAll(".user-modal-link");
        userModalLinks.forEach((link) => {
            if (typeof(link) != 'undefined' && link != null) {
                link.addEventListener("click", function (e) {
                    e.preventDefault();
                    let td = link.closest('td');
                    let userid = td.previousSibling;
                    modalBody.innerHTML = '';
                    fetch("api/route/user/getUserModalBody/" + userid.textContent + "/" + callerid.textContent + "/" + callertoken.textContent, {
                    }).then(function (response) {
                        return response.text();
                    }).then(function (response) {
                        modalBody.innerHTML = response.toString();
                    }).catch((error) => {
                        console.log(error.toString());
                    });
                });
            }
        });
    });

} );


    let dt = $('.table-dt, .table-dten');
    
    console.log(dt);
    
    dt.dataTable({
        "pageLength": 25
    });
   
    let tabledts = document.querySelectorAll('.table-dt, .table-dten');
    console.log(tabledts);
    
    let tabledt = document.querySelector('.table-dt, .table-dten');
    console.log(tabledt);

    
    $('.table-dtfr').DataTable( {
        language: {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        "pageLength": 25

    
    });

    const inputs = document.querySelectorAll('input');
    
    const patterns = {
        login: /[a-zA-Z0-9]/,
        password: /[a-zA-Z0-9]/,
        firstname: /[a-z]/,
        surname: /[a-z]/,
        email: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+\.[a-zA-z]{2,3}$/,
        phonenumber: /^[0-9]/
    };
    

  /**
   * 
   * @param {*} e 
   * @returns 
   */
    function validate(e) {
        var c = e.keyCode;
        var lettre = String.fromCharCode(c);
        var pat = patterns[e.target.attributes.name.value];
        console.log(pat);
      if (lettre.match(pat)) {
        return true;
       
      } else {
        return false;
        
      }
    }

