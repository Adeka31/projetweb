# Examen Projet web

Ce projet est une ébauche de website implémentant la gestion d'un site internet d'une école. 
Il permet l'inscription de personne, le login, le choix des différents cours. 
Ce projet est créer dans le cadre du cours de Projet de Développement Web de la formation Informatique de Gestion de l'institution EAFC Namur-Cadets.

## Installation 

### Prérequis
   
    OS : 
        - Windows
    Serveur de base de bonnées :
        - MySQL
    Serveur Web :
        - Appache 2
        - MySQL 5.7.36
        - PHP 8.1.0

    Gitbash

### Mise en place

Importez le projet via un des liens suivant :
        
        -Clone SSH : git@gitlab.com:Adeka31/projetweb.git
        -Clone HTTPS : https://gitlab.com/Adeka31/projetweb.git
    
Créez un dossier dans /wamp64/www et donnez lui un nom.

Allez dans Gitbash, puis allez dans le dossier que vous venez de créer pour le projet. Une fois fait, 
utilisez la commande suivante plus le liens SSH ou HTTPS mentionné ci-dessus :

```
git clone https://gitlab.com/Adeka31/projetweb.git .
```

Créez une nouvelle base de données dans MySQL grace à la ligne suivante :
    
``` 
CREATE DATABASE nom_db;  
```
    
Retournez dans le projet, copiez le fichier config-dist.php et renomez-le config.php,  puis, modifiez les informations de connexion du fichier avec celle ci-dessous pour correspondre aux informations de votre base de donnée.

```
<?php

const DB_HOST = 'localhost';
const DB_NAME = 'projetweb';
const DB_USER = 'root';
const DB_PASSWORD = '';
const ROOT_PATH = __DIR__;

// alternative syntax of constant declaration
define('ALTERNATIVE_CONST', 'test');

```

Ensuite, via Gitbash, aller dans le dossier migration du projet. Puis utiliez les commandes suivantes :

``` 
php migration.php migration 
``` 
``` 
php migration.php --all 
```


Si cela ne fonctionne pas, faite un copier/coller de chaque table dans la console MySQL.

Vous pouvez maintenant démarrer le site via l'adresse suivante :

```
http://localhost/nom_du_projet/ 
```


### Utilisation

#### Enregistrement

Après l'installation, il n'y a aucun enregistrement en bd. 
La première étape est de créer un utilisateur celui-ci se vera attribué le rôle d'ADMION. 
Attention à cela. Les utilisateurs suivant auront par défaut le rôle de GUEST.

#### Login

Suite à la création de'un utilisateur, on peut alors se connecter sur le site. 
Nous avons alors accès aux différents cours auxquels l'utilisateur peut s'inscrire ainsi qu'à son profile.

#### Profile

Sur la page du profile, nous avons accès aux informations de l'utilisateurs ainsi qu'au différents cours auxquels il est inscrit.
L'utilisqteur a la possibilité de modifier ses informations personnelles via une modale. 
Il peut également exporter son profile sous un format Json.

#### Cours

La page de cours permet à l'utilisateur de s'inscrire à un cours du moment qu'il a les prérequis lui permetant de le faire.