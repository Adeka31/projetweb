<?php

namespace app\Models;

use PDO;
use app\Helpers\Output;
use app\Helpers\Text;
use app\Controllers\Role;

class User extends Model
{
    protected int $id;
    protected string $login;
    protected string $password;
    protected string $email;
    protected string $created;
    protected string $lastlogin;
    protected string $name;
    protected string $surname;
    protected string $birthdate;
    protected string $address;
    protected string $phonenumber;
    protected string $lang;



    public function __construct() {
        parent::__construct();
    } 

    /**
     * 
     * @param array $data
     * @return int
     */

    public static function create(array $data): int
    {
        $insert = self::$connect->prepare("INSERT INTO user (`login`, `password`, firstname, surname, birthdate, `address`, phonenumber, lang, email, lastlogin, created) VALUES(?, ?, ?, ? , ?, ?, ?, ?, ?, Now(), NOW())");
        $insert->execute(array_values($data));
       
        if ($insert->rowCount()) {
            return self::$connect->LastInsertId();
        }
       
        return 0;

    } 



    /**
     *
     * @param string $login
     * @param string $email
     * @return int
     */

    public static function getByUsernameOrEmail(string $login, string $email): int
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE `login` = ? OR email = ?");
        $result->execute([$login, $email]);
        return $result->fetchColumn();
    }


    /**
     * 
     * @param string $orderby
     * @return array
     */

    public static function getAll(string $orderby = ''): array
    {
        $users = [];
        $sql = 'SELECT u.id,
                u.`login`,
                u.email,
                u.created,
                u.lastlogin,
                u.lang,
                u.image,
                r.name
               FROM user u
               JOIN user_role ur ON ur.userid = u.id
               JOIN role r ON r.id = ur.roleid
               ORDER BY u.id';

        $request = self::$connect->prepare($sql);
        $request->execute();
        while ($data_tmp = $request->fetchObject()) {
            $users[] = $data_tmp;
        }
        return $users;
    }

    /**
     * @param int $id
     * @return mixed
     */

    public static function getColorName(int $id): mixed
    {
        $result = self::$connect->prepare("SELECT IF(c.nameC IS NOT NULL, c.nameC, '') as color FROM user u,color c WHERE u.colorid = c.idC AND u.id = ?");
        $result->execute([$id]);
        return $result->fetchObject()->color;
    }

  
    /**
     * @param int $id
     * @return mixed
     */


    public static function isAdmin(int $id): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE roleid = " . Role::ADMIN . " AND userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * @param int $id
     * @param int $role
     * @return mixed
     */

    public static function hasRole(int $id, int $role): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE userid = ? AND roleid = ?");
        $result->execute([$id, $role]);
        return $result->fetchColumn();
    }

    /**
     * @param int $id
     * @param string $role
     * @return mixed
     */

    public static function hasRoleByName(int $id, string $role): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role ur, role r WHERE ur.roleid = r.id AND ur.userid = ? AND r.name = ?");
        $result->execute([$id, $role]);
        return $result->fetchColumn();
    }

    /**
     * @param int $id
     * @return mixed
     */

    public static function hasAnyRole(int $id): mixed
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user_role WHERE userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    /**
     * @param int $id
     * @param int $roleid
     * @return bool
     */
    public static function addRole(int $id, int $roleid): bool
    {
        $request = self::$connect->prepare("INSERT INTO user_role (userid, roleid, created) VALUES (?, ?, NOW())");
        $request->execute([$id, $roleid]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }

    /**
     * @param int $id
     * @param int $roleid
     * @return bool
     */
    public static function updateRole(int $id, int $roleid): bool
    {
        $request = self::$connect->prepare("INSERT INTO user_role (userid, roleid, created) VALUES (?, ?, NOW()) ON DUPLICATE KEY UPDATE roleid = ?");
        $request->execute([$id, $roleid, $roleid]);
        if ($request->rowCount()) {
            return true;
        }
        return false;
    }




}