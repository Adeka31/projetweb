<?php

namespace app\Helpers;

use app\Controllers\Color;
use app\Controllers\Course;
use stdClass;

class Bootstrap
{
    /**
     *
     * @param array|object $data    
     * @param array|object $cols    
     * @param string $title        
     * @param string $class         
     * @return string
     */

    public static function table(array|object $data, array|object $cols, string $title = '', string $class = ''): string
    {
        $thead = '';
        $tbody = '';
        if (is_array($cols) || is_object($cols)) {
            foreach ($cols as $th) {
                $thead .= '<th>' . $th . '</th>';
            }
        } else {
            $thead = '<th>' . $cols . '</th>';
        }
        if (is_array($data) || is_object($data)) {
            foreach ($data as $sub) {
                $tbody .= '<tr>';
                if (is_array($sub) || is_object($sub)) {
                    foreach ($sub as $value) {
                        $tbody .= '<td>' . $value . '</td>';
                    }
                } else {
                    $tbody .= '<td>' . $sub . '</td>';
                }
                $tbody .= '</tr>';
            }
        } else {
            $tbody .= '<tr><td>' . $data . '</td></tr>';
        }

        return '<h2>' . $title . '</h2><table class="table table-striped ' . $class . '"><thead><tr>' . $thead . '</tr></thead><tbody>' . $tbody . '</tbody></table>';
    }

    /**
     *
     * @param object|array $options
     * @param string $selected
     * @return string
     */

    public static function getFormOptions(object|array $options, string $selected = ''): string
    {
        $output = '';
        foreach ($options as $key => $value) {
            if ($key == $selected) {
                $attr = 'selected';
            } else {
                $attr = '';
            }
            $output .= '<option value="' . $key . '" ' . $attr . '>' . $value . '</option>';
        }
        return $output;
    }

    /**
     *
     *
     * @see Bootstrap::profile()
     * @see Output::render()
     * @param object $data     
     * @param string $class     
     * @return string
     */

    public static function profile(object $data, string $class = ''): string
    {

        //création du profile et de la modale update
        
        $tbody = '';
        $modal = '';
        $modalBody = '<form action="index.php?view=api/user/update" method="post" enctype="multipart/form-data">';
        $vars = "";
        foreach ($data as $key => $value) {
            $type = '';
            $modif = '';
            if ($key == 'image') {
                $type = 'file';
                $content = '<div class="row">
                            <div class="col-6">
                                <img src="' . $value . '?' . time() . '" alt="Photo" class="d-block img-fluid" id="profile-photo">
                            </div>
                          </div>';
                $modalBody .= $content;
                $tbody .= $content;
                $value = "";
                $key = "photo";
            }


            else if(in_array($key, ['login', 'firstname', 'surname', 'address'])){
                $type='text';
            }
            else if($key == 'email'){
                $type = 'email';
            }
            else if($key == 'password'){
                $type = 'password';
                $value ='';
                
            }
            else if($key == 'birthdate'){
                $type = 'date';
            }
            else if($key == 'phonenumber'){
                $type = 'number';
            }
            else if(in_array($key, ['created', 'lastlogin', 'updated', 'lang'])){
                $type = 'text';
                $modif = 'disabled';
            }
            else if($key == 'id'){
                $type = 'hidden';
                
            }


            if($key != 'id'){
                $modalBody .= '<label for="uu-' . $key . '">' . $key . '</label>';
                $tbody .= '<tr><th>' . Text::getStringFromKey($key) . '</th><td>' . $value . '</td></tr>';
            }
            $modalBody .= '<input type="' . $type . '" id="uu-' . $key . '" name="' . $key . '" class="form-control" ' . $modif . ' value="' . $value . '" onkeypress="return validate(event);">';
        }

        $modalBody .= '<button id="btn_modal_update"  class="btn btn-sm btn-primary" onclick=""> Update </button> </form>';
        $modal .= self::viewModal('modalupdate', 'update', $modalBody, '', 'lg');
       
        return '<h2>' . Text::getStringFromKey('profile') . '</h2>
                <a href="index.php?view=api/user/exportProfile/' . $_SESSION['userid'] . '/true" class="btn btn-sm btn-primary">Exporter</a>
                <table class="table ' . $class . '">' . $tbody . '</table>
                <button id="btn_update"  class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#modalupdate" > Update </button>' .$modal
                ;

        
        
    }


    /**
     * @param object $data
     * @return void
     */

    public static function exportProfile(object $data): void
    {
        ob_clean();
        $filename = $data->login . '_' . time() . '.json';
        
        header('Content-type: application/json');
        header('Content-disposition: attachment; filename="' . $filename . '"');

        if ($data->format == 'json'){

            unset($data->format);
            $j = json_encode($data, JSON_PRETTY_PRINT);
            echo $j;
            exit();

        } else {
            unset($data->format);
            foreach ($data as $key => $value){
                   echo $key .' : '. $value . "\n";  
            }
        }

        
    }


    /**
     * 
     * @param object $data
     * @return void
     */


    public static function exportprofiletotxt(object $data): void
    {

          $data = json_decode(json_encode($data), true);
          $filename = 'courses_list_' . time() . '.txt';
          
          $ressource = fopen($filename, 'w');
          foreach ($data as $course) {
             
              fputcsv($ressource, $course);

          }
         
          fclose($ressource);
  
      
        header('Content-Type: text/txt');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        
        echo file_get_contents($filename);
        
        unlink($filename);
    }


    /**
     * @param array $data
     * @return void
     */

    public static function exportCourseList(array $data): void
    {
        
        $data = json_decode(json_encode($data), true);
        $filename = 'courses_list_' . time() . '.csv';

        $ressource = fopen($filename, 'w');
        foreach ($data as $course) {
           
            fputcsv($ressource, $course);
        }
        
        fclose($ressource);

        
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        
        echo file_get_contents($filename);
        
        unlink($filename);

    }

    /**
     *
     * @param object $data
     * @return string
     */
/* 
    public static function profileUpdate(object $data): string
    {
        $lang = new stdClass();
        $lang->en = 'English';
        $lang->fr = 'Français';

       
 
        return '<hr>
                 <h3><a href="#profile-update-collapse" data-bs-toggle="collapse" role="button" class="text-decoration-none">' . Text::getStringFromKey('update') . '</a></h3>
                 <div class="collapse" id="profile-update-collapse">
                    <form action="index.php?view=api/user/update" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="uu-userid" name="id" value="' . $data->id . '">
                        <label for="uu-password">' . Text::getStringFromKey('password') . '</label>
                        <input type="password" id="uu-password" name="password" class="form-control" placeholder="Pour changer votre mot de passe, cliquez ici">
                        <label for="uu-email">' . Text::getStringFromKey('email') . '</label>
                        <input type="email" id="uu-email" name="email" class="form-control" value="' . $data->email . '">
                        <label for="uu-lang">' . Text::getStringFromKey('lang') . '</label>
                        <select name="lang" id="uu-lang" class="form-control">
                            ' . self::getFormOptions($lang, $data->lang) . '
                        </select>
                        <label for="uu-photo">' . Text::getStringFromKey('photo') . '</label>
                        <p><input type="file" id="uu-photo" name="photo"></p>
                        <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                    </form>
                 </div>';


        
    } */

    /**
     * @param array $courses
     * @return string
     */

    public static function profileCourses(array $courses): string
    {
        $body = '';
        foreach ($courses as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {
                if ($key == 'det') {
                    $value = Text::yesOrNo($value);
                } elseif ($key == 'courseid') {
                    continue;
                }
                $body .= '<td>' . $value . '</td>';
            }
            $body .= '</tr>';
        }
        return '<hr><h2><a href="#profile-courses-list" data-bs-toggle="collapse" role="button" class="text-decoration-none">' . Text::getStringFromKey('courses') . '</a></h2>
                <table class="table table-striped collapse" id="profile-courses-list">
                    <thead>
                        <tr>
                            <th>' . Text::getString(['formation', 'formation']) . '</th>
                            <th>' . Text::getString(['course', 'cours']) . '</th>
                            <th>' . Text::getString(['periods', 'périodes']) . '</th>
                            <th>' . Text::getString(['determining', 'déterminant']) . '</th>
                            <th>' . Text::getString(['prerequisite', 'prérequis']) . '</th>
                            <th>' . Text::getString(['teacher', 'professeur']) . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>
                <a href="" class="btn btn-sm btn-primary">UP</a>';
    }

    /**
     *
     * @link http://localhost/web/js/main.js
     * @param array $data
     * @return string
     */
    public static function courses(array $data): string
    {
        
        $body = '';
        $modal = '';
        
        foreach ($data as $row) {
            $course = new Course();

            $body .= '<tr>';
            
            foreach ($row as $key => $value) {
                
                if ($key == 'det') {
                    $value = Text::yesOrNo($value);
                } elseif ($key == 'courseid') {
                    continue;
                }
                $body .= '<td>' . $value . '</td>';
            }
             if (!$course->getEnrol($row->courseid, $_SESSION['userid'])) {
                $body .= '<td><a href="index.php?view=api/course/enrol/' . $row->courseid . '/' . $_SESSION['userid'] . '" class="btn btn-sm btn-success">+</a></td>';
            } else {
                $body .= '<td>Déjà inscrit</td>';
            }
            $body .= '</tr>'; 
           
        }
       
        return '<h2>' . Text::getStringFromKey('courses') . '</h2>
                <table class="table table-striped table-dt' . $_SESSION['lang'] . '" id="courses-list">
                    <thead>
                        <tr>
                            <th>' . Text::getString(['formation', 'formation']) . '</th>
                            <th>' . Text::getString(['course', 'cours']) . '</th>
                            <th>' . Text::getString(['periods', 'périodes']) . '</th>
                            <th>' . Text::getString(['determining', 'déterminant']) . '</th>
                            <th>' . Text::getString(['prerequisite', 'prérequis']) . '</th>
                            <th>' . Text::getString(['teacher', 'professeur']) . '</th>
                            <th>' . Text::getString(['enrol', 'inscrire']) . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>' . $modal;

    }

    /**
     *
     * @see Output::render()
     * @param string $message
     * @param string $class    
     */

    public static function messageBox(string $message, string $class = 'danger'): string
    {
        return '<div class="alert alert-' . $class . '">' . $message . '</div>';
    }

    /**
     * @param string $content
     * @param string $type
     * @param bool $dismiss
     * @return string
     */

    public static function alert(string $content, string $type = 'success', bool $dismiss = false): string
    {
        if ($dismiss) {
            return '<div class="alert alert-' . $type . ' alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              ' . $content . '
            </div>';
        } else {
            return '<div class="alert alert-' . $type . '">' . $content . '</div>';
        }
    }


    /**
     * @param string $link
     * @param string $caption
     * @param string $class
     * @return string
     */

    public static function linkBtn(string $link, string $caption, string $class = 'default'): string
    {
        return '<a href="' . $link . '" class="btn btn-' . $class . '">' . $caption . '</a>';
    }

    /**
     * @param string $link
     * @param string $caption
     * @param string $target
     * @return string
     */

    public static function link(string $link, string $caption, string $target = 'default'): string
    {
        return '<a href="' . $link . '" class="lien" target="' . $target . '">' . $caption . '</a>';
    }

    /**
     *
     * @param string $target_id
     * @param string $btn_text
     * @param string $btn_class
     * @return string
     */

    public static function btnModal(string $target_id, string $btn_text, string $btn_class = 'primary'): string
    {
        return '<button type="button" class="btn btn-' . $btn_class . '" data-bs-toggle="modal" data-bs-target="#' . $target_id . '">' . $btn_text . '</button>';
    }

    /**
     * Link Modal bootstrap
     *
     * @param string $target_id
     * @param string $caption
     * @param string $class
     * @return string
     */

    public static function linkModal(string $target_id, string $caption, string $class = ''): string
    {
        return '<a href="#" class="link ' . $class . '" data-bs-toggle="modal" data-bs-target="#' . $target_id . '">' . $caption . '</a>';
    }


    /**
     * Générateur de Modal bootstrap
     *
     * @param string $modal_id
     * @param string $modal_title
     * @param string $modal_body
     * @param string $modal_footer
     * @param string $modal_size
     * @return string
     */

    public static function viewModal(string $modal_id, string $modal_title, string $modal_body, string $modal_footer = '', string $modal_size = ''): string
    {
        if ($modal_size == 'lg') {
            $modal_size = ' modal-lg';
        } elseif ($modal_size == 'hg') {
            $modal_size = ' modal-hg';
        }
        return '<div class="modal fade" tabindex="-1" id="' . $modal_id . '" aria-hidden="true">
          <div class="modal-dialog' . $modal_size . '">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">' . $modal_title . '</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                ' . $modal_body . '
              </div>
              <div class="modal-footer">
                ' . $modal_footer . '
              </div>
            </div>
          </div>
        </div>';
    }

    /**
     * @param string $title
     * @param string $text
     * @param string $footer
     * @param string $style
     * @param string $class
     * @param string $id
     * @return string
     */
    
    public static function showCard(string $title, string $text, string $footer = '', string $style = '', string $class = '', string $id = ''): string
    {
        if ($footer) {
            $footer = '<div class="card-footer">' . $footer . '</div>';
        }
        return '<div class="card card-war ' . $class . '" style="' . $style . '" id="' . $id . '">
                <div class="card-header">' . $title . '</div>
                <div class="card-body">' . $text . '</div>
                ' . $footer . '
            </div>';
    }
}