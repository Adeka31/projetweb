<?php

namespace app\Helpers;

use PDO;
use PDOException;

class DB
{
    protected static PDO|null $dbh = null;

    /**
     * @return PDO|null
     */
    
    public static function connect(): PDO|null
    {
        if (!self::$dbh) {

            try {
               
                require_once __DIR__ . '/../../config.php';
                self::$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8', DB_USER, DB_PASSWORD);
                self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            } catch (PDOException $exception) {
                
                die ('Error : ' . $exception->getMessage());
            }
        }
        return self::$dbh;
    }
}