<?php

namespace app\Helpers;

use app\Helpers\Bootstrap;

class Output
{
    /**
     *
     * @param string $view
     * @return void
     * @throws \ReflectionException
     * @throws \Exception
     */

    public static function getContent(string $view): void
    {
        
        $exts = ['html', 'php'];
        
        $route = parse_url($_SERVER['REQUEST_URI']);
        
        if (str_contains($route['query'], 'view=api/')) {
            $params = [];
            
            $elements = explode('/', rtrim($route['query'], '/'));
            
            foreach ($elements as $key => $value) {
                if ($key == 1) {
                    
                    $class = 'app\Controllers\\' . ucfirst($value);
                } elseif ($key == 2) {
                    $method = $value;
                } elseif ($key == 0) {
                    
                    continue;
                } else {
                   
                    $params[] = $value;
                }
            }
            if (!empty($class) && !empty($method)) {
               
                $r = new \ReflectionMethod($class, $method);
                $nbr = $r->getNumberOfParameters();
                if ($nbr != count($params)) {
                    
                    throw new \Exception('Parameters count mismatch');
                }
                
                $controller = new $class();
                
                is_callable($method, true, $callable_name);
               
                $controller->{$callable_name}(...array_values($params));
            }
        } elseif (!empty($view)) {
            foreach ($exts as $ext) {
                
                $complete_path = $view . '.' . $ext;
                
                if (file_exists($complete_path)) {
                   
                    include_once $complete_path;
                }
            }
        } else {
           
            header('Location: index.php?view=view/default');
         
            die;
        }
    }

    /**
     *
     * @see Bootstrap
     * @param string $template          
     * @param object|array|string $data 
     * @param string $class             
     * @return void
     */

    public static function render(string $template, object|array|string $data, string $class = 'danger')
    {
        echo Bootstrap::$template($data, $class);
    }

    /**
     *
     * @param string $text      
     * @param string $level     
     * @param string $redirect  
     * @return void
     */

    #[NoReturn] public static function createAlert(string $text, string $level = 'info', string $redirect = 'index.php?view=view/default'): void
    {
        $_SESSION['alert'] = $text;
        $_SESSION['alert_level'] = $level;
        header('Location: ' . $redirect);
        die;
    }

    /**
     * 
     * @see self::checkAlertLevel()
     * @return void
     */

    public static function manageAlerts(): void
    {
        if (!empty($_SESSION['alert']) && !empty($_SESSION['alert_level'])) {
            echo '<div class="alert alert-' . self::checkAlertLevel() . '">' . $_SESSION['alert'] . '</div>';
            unset($_SESSION['alert']);
            unset($_SESSION['alert_level']);
        }
    }

    /**
     *
     * @return string
     */
    
    public static function checkAlertLevel(): string
    {
        if (!empty($_SESSION['alert_level'])) {
            // Principe de la "white list"
            if (in_array($_SESSION['alert_level'], ['success', 'danger', 'info'])) {
                return $_SESSION['alert_level'];
            } else {
                return 'info';
            }
        }
        return '';
    }
}