<?php

namespace app\Helpers;

class Access
{
    /**
     * 
     *
     * @return bool
     */

    public static function isLoggedIn() : bool
    {
        if (!empty($_SESSION['userid'])) {
            return true;
        } else { 
            return false;
        }
    }

    /**
     *
     *
     * @return void
     */

    public static function checkLoggedIn() : void
    {
        if (empty($_SESSION['userid'])) {
            header('Location: index.php?view=view/user/login');
            die;
        }
    }

    /**
     *
     * @param int $id  
     * @return void
     */


    public static function checkProfile(int $id) : void
    {
        if (empty($_SESSION['userid']) || $_SESSION['userid'] != $id) {
            header('Location: index.php?view=view/user/login');
            die;
        }
    }

    /**
     *
     * @param string $location 
     * @return void
     */
    public static function redirect(string $location = 'index.php') : void
    {
        header('Location: ' . $location);
        die;
    }
}