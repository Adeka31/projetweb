<?php

namespace app\Controllers;

use app\Helpers\Helper;
use app\Helpers\Output;
use app\Helpers\Access;
use app\Helpers\Text;


class User extends Controller
{

    protected static array $images = [
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/jfif',
    ];


    /**
     * 
     * @return void
     */


    public function create(): void
    {
       

        // viérifie que la requète vient d'une méthode _POST de l'html.
        if (!$_POST) {
            header('HTTP/1.1 405');
        }


        if (!empty($_POST['login']) && !empty($_POST['password']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) 
        && !empty($_POST['firstname']) && !empty($_POST['surname']) && !empty($_POST['birthdate']) && !empty($_POST['address'])
        && !empty($_POST['phonenumber'] && !empty($_POST['lang'])))
        {
            $user_data = [
                'login' => trim(htmlentities(($_POST['login']), ENT_QUOTES)),
                'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                'firstname' => trim(htmlentities(($_POST['firstname']), ENT_QUOTES)),
                'surname' => trim(htmlentities(($_POST['surname']), ENT_QUOTES)),
                'birthdate' => trim(htmlentities(($_POST['birthdate']), ENT_QUOTES)),
                'address' => trim(htmlentities(($_POST['address']), ENT_QUOTES)),
                'phonenumber' => trim(htmlentities(($_POST['phonenumber']), ENT_QUOTES)),
                'lang' => htmlentities(($_POST['lang']), ENT_QUOTES),
                'email' => htmlentities(($_POST['email']), ENT_QUOTES)
        
            ];

            //compare les données login et email pour vérifier que l'utilisateur n'existe pas
             if (!$this->model->getByUsernameOrEmail($user_data['login'], $user_data['email'])) {
                // crée l'utilisateur en DB et stocke l'id dans $userid
                $userid = $this->model->create($user_data);
                
    
                if ($userid) {
                    
                    //crée l'image du profil
                    $photo_message = false;
                    if (!empty($_FILES['photo']['name'])) {
                        $user = $this->get($userid);
                        if (!$this->manageUploadedFile($user, true)) {
                            $photo_message = 'L\'import de la photo a échoué';
                        }
                    }


                    //si la db est vide, le premier utilisateur est ADMIN
                    if(empty($this->model->getAll())) $userRole = Role::ADMIN;
                    else $userRole = Role::GUEST;

                    //assigne GUEST comme role par défaut 
                    $this->addRole($userid, $userRole);
               
                    if ($this->model->hasAnyRole($userid)){
                        Output::render('messageBox', 'Utilisateur ' . $_POST['login'] . ' avec l\'adresse email ' . $_POST['email'] . ' créé avec succès', 'success');


                    }else {
                        
                        Output::render('messageBox', 'La création du compte utilisateur a partiellement échoué');
                    }

                } else {
                    Output::render('messageBox', 'La création du compte utilisateur a échoué');
                }
            } else {
                Output::render('messageBox', 'Un utilisateur avec cet identifiant ou cette adresse email existe déjà !');
            }
        } else {

            header('Location: index.php?view=user/signup');
            die;
        }

           
    }
        

    /**
     * @param int $id
     * @param bool $render
     * @return void
     * @throws \Exception
     */

    public function exportProfile(int $id, bool $render = true): void
    {
        $user = $this->getForProfile($id);
        $userForProfile = self::formatForProfile($user);
        $userForProfile->format = 'json';

        if ($render) {
            Output::render('exportProfile', $userForProfile);
        } else {
            echo json_encode($userForProfile);
        }
    }

    

     /**
     * @param int $id
     * @return void
     * @throws \Exception
     */
    public function exportImage(int $id): void
    {
        $user = $this->getForProfile($id);
        $userForProfile = self::formatForProfile($user);

        Output::render('exportImage', $userForProfile);
    }

    

    /**
     * @param int $id
     * @return bool
     */

    public function isAdmin(int $id) : bool
    {
        return $this->model->isAdmin($id);
    }

    /**
     * @param int $id
     * @param int $roleid
     * @return bool
     */

    public function hasRole(int $id, int $roleid): bool
    {
        return $this->model->hasRole($id, $roleid);
    }

    /**
     * @param int $id
     * @param int $roleid
     * @return void
     */

    public function updateRole(int $id, int $roleid): void
    {
        $this->model->updateRole($id, $roleid);
    }

    /**
     * @param int $id
     * @param int $roleid
     * @return void
     */

    public function addRole(int $id, int $roleid): void
    {
        if (!$this->model->hasAnyRole($id)) {
            $this->model->addRole($id, $roleid);
        }
    }


    /**
     * @param int $id
     * @return mixed
     */
    protected function getForProfile(int $id): mixed
    {
        // récupère les données du user en db
        $user = $this->get($id);
       
        return $user;
    }

    /**
     *
     * @param object $user
     * @return object
     * @throws \Exception
     */

    protected function formatForProfile(object $user): object
    {                                                                                                                              
        $userForProfile = clone $user;
        
        unset($userForProfile->id);
    
        if ($userForProfile->lang == 'fr') {
            $userForProfile->created = date_format( new \DateTime($userForProfile->created),"d/m/Y H\hi");
            $userForProfile->lastlogin = date_format( new \DateTime($userForProfile->lastlogin),"d/m/Y H\hi");
        }
 
        return $userForProfile;
    }

    /**
     *
     * @param int|object $user
     * @param bool $update
     * @return mixed
     * @throws \Exception
     */


    protected function manageUploadedFile(int|object $user, bool $update = false): mixed
    {
        if  (!is_object($user)) {
                $user = $this->model->get($user);
        }

        if  (!empty($_FILES['photo']['tmp_name']) &&
            !empty($_FILES['photo']['name']) &&
            is_uploaded_file($_FILES['photo']['tmp_name']) &&
            in_array($_FILES['photo']['type'], self::$images)) 
            
            {
                $photopath = 'image' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR;
                
                $imagepath = getcwd() . DIRECTORY_SEPARATOR . $photopath;
                
                if (!is_dir($imagepath)) {
                    mkdir($imagepath, 0755, true);
                }
                
                $ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);
                $dest = $imagepath . $user->id . '.' . $ext;
                $url = $photopath . $user->id . '.' . $ext;
                
                $move = move_uploaded_file($_FILES['photo']['tmp_name'], $dest);
                if ($move) {
                   
                    if (!empty($user->image) && $url != $user->image && pathinfo($user->image, PATHINFO_EXTENSION) != $ext) {
                        $existing_file_image = ROOT_PATH . DIRECTORY_SEPARATOR . $user->image;
                        if (file_exists($existing_file_image)) {
                            unlink($existing_file_image);
                        }
                    }
                    $user->image = $url;
                    if ($update) {
                        return $this->model->update($user);
                    }
                } else {
                    throw new \Exception(Text::getString(['Upload file error', 'Erreur de téléchargement du fichier']));
                }

            } else {
            $output_txt = [
                'File error.',
                'Le fichier a été refusé.',
            ];
            if (!empty($_FILES['photo']['type']) && !in_array($_FILES['photo']['type'], self::$images)) {
                $output_txt[0] .= ' File extension must be ' . implode(' ou ', self::$images) . '.';
                $output_txt[1] .= ' Le format de fichier doit être ' . implode(' ou ', self::$images) . '.';
            }
            if ($_FILES['photo']['error']) {
                switch ($_FILES['photo']['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $output_txt[0] .= ' No file sent.';
                        $output_txt[1] .= ' Aucun fichier envoyé.';
                        break;
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        $output_txt[0] .= ' File size must be lower than ' . Helper::getMaxFileSizeHumanReadable() . '.';
                        $output_txt[1] .= ' La taille du fichier doit être inférieure à ' . Helper::getMaxFileSizeHumanReadable() . '.';
                        break;
                    default:
                        $output_txt[0] .= ' Unknown error.';
                        $output_txt[1] .= ' Erreur inconnue.';
                }
            }
            throw new \Exception(Text::getString($output_txt));
        }
        return $user->image;
    }




    
    /**
     * @return void
     * @throws \Exception
     */

    public function update(): void
    {


        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        Access::checkProfile($_POST['id']);

        $langupdate = false;
        $errors = "";
        $user = $this->get($_POST['id']);


        // vérifie les données entrées dans l'update puis appel la méthode update 
        if (!empty($_POST['email']) && filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL) && $_POST['email'] != $user->email) {
            $email = trim($_POST['email']);
            if (!$this->model->getByUsernameOrEmail('', $email)) {
                $user->email = $email;
            }
            else
                $errors .= "<li>Cette addresse email existe déjà!</li>";
        }

        if (!empty($_POST['password']) && $_POST['password'] != $user->password) {
            $user->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        }

        if (!empty($_POST['login']) && $_POST['login'] != $user->login) {
            $user->login = trim($_POST['login']);
        }

        if (!empty($_POST['firstname']) && $_POST['firstname'] != $user->firstname) {
            $user->firstname = trim($_POST['firstname']);
        }

        if (!empty($_POST['surname']) && $_POST['surname'] != $user->surname) {
            $user->surname = trim($_POST['surname']);
        }

        if (!empty($_POST['address']) && $_POST['address'] != $user->address) {
            $user->address = trim($_POST['address']);
        }

        if (!empty($_POST['phonenumber']) && $_POST['phonenumber'] != $user->phonenumber) {
            $user->phonenumber = trim($_POST['phonenumber']);
        }

        if (!empty($_FILES['photo']) && !empty($_FILES['photo']['name'])) {
            try {
                $user->image = $this->manageUploadedFile($user);
            } catch (\Exception $e) {
                $errors .= "<li>" . $e->getMessage() . "</li>";
            }
        }
        if (!$errors) {
            $now = new \DateTime();
            $user->updated = $now->setTimezone(new \DateTimeZone('Europe/Paris'))->format('Y-m-d H:i:s');
        }

        $updateResult = $this->model->update($user);
        if (!$errors && $updateResult) {
            Output::createAlert('La mise à jour du compte utilisateur a été effectuée avec succès.' . 'success', 'index.php?view=api/user/profile/' . $user->id);
        } else {
            $msg = "La mise à jour du compte utilisateur a échoué:";
            if($errors)
                $msg .= "<ul>$errors</ul>";
            else
                $msg .= $user->$updateError;
            Output::createAlert($msg, 'danger', 'index.php?view=api/user/profile/' . $user->id);
        }
    }


    /** 
     *
     * @see \app\Models\User::getByField()
     * @see \app\Models\User::updateFieldById()
     * @return void
     * @throws \Exception
     */

    public function login(): void
    {
        if (!$_POST) {
            header('HTTP/1.1 405');
        }

        if (!empty($_POST['login']) && !empty($_POST['password'])) {
            
            //récupère les données du user correspondant au login
            $user = $this->model->getByField('login', trim($_POST['login']));

            if (!$user) {
                Output::createAlert('Cet utilisateur n\'existe pas!', 'danger', 'index.php?view=view/user/login');
            }

            
            if (password_verify($_POST['password'], $user->password)) {
                
                if ($this->model->updateFieldById('lastlogin', 'NOW()', $user->id)) {
                    
                    session_destroy();
                    session_name('WEB' . date('Ymd'));
                    session_id(bin2hex(openssl_random_pseudo_bytes(32)));
                    session_start(['cookie_lifetime' => 3600]);
                    

                    $_SESSION['userid'] = $user->id;
                    $_SESSION['lang'] = $user->lang;
                    

                    Output::createAlert('Bienvenue ' . $user->login, 'success', 'index.php?view=api/user/profile/' . $user->id);
                }
            } else {
                Output::render('messageBox', 'Paramètres invalides!');
            }
        }
    }


    /**
     * 
     * @return void
     * 
     */

    public function logout(): void
    {
        session_unset();
        session_destroy();
        session_write_close();
        header('Location: index.php');
        die;
    }



    /**
    * @param int $id
    * @return void
    */

    public function profile(int $id): void
    {

        Access::checkProfile($id);

        $user = $this->getForProfile($id);


        Output::render('profile', $user);

     
        $course = new Course();
        $courses = $course->getByUserEnrol($id);
        Output::render('profileCourses', $courses);
    }


}

