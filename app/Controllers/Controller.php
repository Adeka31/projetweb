<?php

namespace app\Controllers;

use stdClass;
use app\Models\Model;


abstract class Controller
{
    protected int $id;
    protected mixed $data;
    protected Model $model;
    protected static mixed $static_model = null;



    /**
     *
     * @param int|null $id
     */

    public function __construct(int $id = null)
    {
        $class = get_called_class();
        $data = explode('\\', $class);
        $class = '\app\Models\\' . end($data);
        $this->model = new $class();
        self::$static_model = $this->model;
            if (!empty($id)) {
                $this->id = $id;
                $this->data = $this->model->get($id);
            }
    }

    /**
     * va chercher les infos en db via l'id
     *
     * @param int $id
     * @return mixed
     */

    public function get(int $id): mixed
    {
        return $this->model->getByField('id', $id);
    }


    /**
     *
     * @param stdClass $object
     * @return void
     */
    
    public function import(stdClass $object): void
    {
        foreach (get_object_vars($object) as $key => $value) {
            $this->$key = $value;
        }
    }

}