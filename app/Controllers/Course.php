<?php

namespace app\Controllers;

use app\Helpers\Bootstrap;
use app\Helpers\Output;
use app\Helpers\Access;

class Course extends Controller
{
    /**
     * @return void
     */

    public function list(): void
    {
        Access::checkLoggedIn();

        $courses = $this->model->getAll();
        
        Output::render('courses', $courses);

    }

    /**
     * @param int $courseid
     * @param int $userid
     * @return int
     */

    public function getEnrol(int $courseid, int $userid): int
    {
        return $this->model->getEnrol($courseid, $userid);
    }

    /**
     * @param int $userid
     * @return array
     */

    public function getByUserEnrol(int $userid): array
    {
        return $this->model->getByUserEnrol($userid);
    }

    /**
     * @param int $courseid
     * @param int $userid
     * @return void
     */

    public function enrol(int $courseid, int $userid): void
    {
        if (self::getEnrol($courseid, $userid)) {
            Output::createAlert('Utilisateur déjà inscrit à ce cours', 'danger', 'index.php?view=api/course/list');
        } else {
           
            $prereq = $this->model->getPreprequisite($courseid);
            if ($prereq && !self::getEnrol($prereq, $userid)) {
                Output::createAlert('L\'inscription a échoué, les prérequis ne sont pas satisfaits', 'danger', 'index.php?view=api/course/list');
            }
            
            if ($this->model->enrol($courseid, $userid)) {
                Output::createAlert('Utilisateur inscrit avec succès', 'success', 'index.php?view=api/course/list');
            } else {
                Output::createAlert('L\'inscription a échoué', 'danger', 'index.php?view=api/course/list');
            }
        }
    }

    /**
     * @param int $formationid
     * @return void
     */

    public function getByFormationForForm(int $formationid): void
    {
        $data = $this->model->getByFormationForForm($formationid);
        Output::render('getFormOptions', $data);
    }

    /**
     * @param int $id
     * @return void
     */

    public function getForForm(int $id): void
    {
        $data = $this->model->getByFormation($id);
        Output::render('getFormOptions', $data);
    }

    /**
     * @return void
     */
    
    public function createCourse(): void
    {
        Access::checkAdmin();

        $formation = new Formation();
        $data = $formation->model->getAllForForm('name');
        Output::render('createCourse', $data);
    }
}