<?php

const DB_HOST = 'localhost';
const DB_NAME = '';
const DB_USER = '';
const DB_PASSWORD = '';
const ROOT_PATH = __DIR__;

// alternative syntax of constant declaration
define('ALTERNATIVE_CONST', 'test');
