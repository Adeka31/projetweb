CREATE TABLE IF NOT EXISTS `user_country` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userid` int(10) UNSIGNED NOT NULL,
    `countryid` tinyint(4) UNSIGNED NOT NULL,
    `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `userid` (`userid`, `countryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;