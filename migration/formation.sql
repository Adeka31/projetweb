CREATE TABLE IF NOT EXISTS `formation` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO `formation` (`id`, `name`) VALUES
(1, 'Webdeveloper'),
(2, 'Bac Informatique de gestion');