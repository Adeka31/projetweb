CREATE TABLE IF NOT EXISTS `country` (
    `id` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(124) NOT NULL,
    `code` varchar(4) NOT NULL,
    `created` datetime NOT NULL,
    `updated` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`, `code`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO `country` (`id`, `name`, `code`, `created`, `updated` ) VALUES
    (1,'Belgique', 'BE', NOW(), NULL),
    (2, 'France', 'FR', NOW(), NULL),
    (3, 'Allemagne', 'GE', NOW(), NULL),
    (4, 'Pays-Bas', 'DE', NOW(), NULL),
    (5, 'Italie', 'IT', NOW(), NULL);