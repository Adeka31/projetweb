CREATE TABLE IF NOT EXISTS `group` (
     `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
     `name` varchar(255) NOT NULL,
     `year` int(4) unsigned NOT NULL,
     `created` datetime NOT null,
     `updated` datetime NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

