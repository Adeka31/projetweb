CREATE TABLE IF NOT EXISTS `user` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `login` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `firstname` varchar(255) NOT NULL,
    `surname` varchar(255) NOT NULL,
    `birthdate` date NOT NULL,
    `address` varchar(255) NOT NULL,
    `phonenumber` varchar(255) NOT NULL,
    `image` varchar(255) NULL,
    `lang`varchar(5) NOT NULL,
    `email` varchar(255) NOT NULL,
    `lastlogin` datetime NOT NULL,
    `created` datetime NOT NULL,
    `updated` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`),
    UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
