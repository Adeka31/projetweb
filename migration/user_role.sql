CREATE TABLE IF NOT EXISTS `user_role` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userid` int(10) UNSIGNED NOT NULL,
    `roleid` tinyint(4) UNSIGNED NOT NULL,
    `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `userid` (`userid`, `roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;